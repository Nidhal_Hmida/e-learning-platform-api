package elerning.platform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ELerningPlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(ELerningPlatformApplication.class, args);
	}


}
