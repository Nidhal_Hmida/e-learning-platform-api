package elerning.platform.Repository;

import elerning.platform.Model.Event;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface EventRepository  extends JpaRepository<Event, Long>{
}




