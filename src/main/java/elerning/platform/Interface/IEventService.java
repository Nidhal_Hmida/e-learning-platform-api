package elerning.platform.Interface;

import elerning.platform.Model.Event;

import java.util.List;

public interface IEventService {

    public Event addEvent(Event event);
    public List<Event>  getAllEvents();
    public Event editEvent (long id,Event event);
    public void deleteEvent(long id);

}
