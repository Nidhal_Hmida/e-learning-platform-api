package elerning.platform.Service;

import elerning.platform.Exceptions.ResourceNotFoundException;
import elerning.platform.Interface.IEventService;
import elerning.platform.Model.Event;
import elerning.platform.Repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service

public class EventService implements IEventService {

    @Autowired
    private EventRepository repository ;

    public boolean verifyDate(LocalDate date1 )
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd");

        return (date1.isBefore( LocalDate.now()));
    }

    @Override
    public Event addEvent(Event event)
    {
        return repository.save(event) ;
    }

    @Override
    public List<Event> getAllEvents() {
        return repository.findAll();
    }

    @Override
    public Event editEvent(long id,Event event)
    {
        Event existedEvent = repository.findById(id)
             .orElseThrow(() -> new ResourceNotFoundException("event not found on :: " + event.getId()));

        event.setId(id);
        return repository.save(event);
    }

    @Override
    public void deleteEvent(long id)
    {
        repository.findById(id)
               .orElseThrow(() -> new ResourceNotFoundException("event not found on :: " + id));
        repository.deleteById(id);
    }


}
