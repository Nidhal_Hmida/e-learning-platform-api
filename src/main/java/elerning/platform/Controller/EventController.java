package elerning.platform.Controller;

import elerning.platform.Model.Event;
import elerning.platform.Service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.*;


@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*")
public class EventController {

    @Autowired
    private EventService service;

    @GetMapping("/events")
    public List<Event> getAllEvents() {
        return service.getAllEvents();
    }

    @PostMapping("/events")
    public ResponseEntity<?> addEvent(@RequestBody @Valid Event event) {


        Event createdEvent = service.addEvent(event);
        return new ResponseEntity<>(createdEvent, HttpStatus.OK);
    }

    @PutMapping("/events/{id}")
    public ResponseEntity<?> updateTask(@PathVariable(value = "id") long eventId, @Valid @RequestBody Event event) {
        Event updatedEvent=null ;

            updatedEvent = service.editEvent(eventId, event);
            return new ResponseEntity<>(updatedEvent, HttpStatus.OK);




    }

    @DeleteMapping("/events/{id}")
    public  ResponseEntity<?> deleteTask(@PathVariable(value = "id") long eventId) {

        service.deleteEvent(eventId);
        return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);

    }
}
