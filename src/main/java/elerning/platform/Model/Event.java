package elerning.platform.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity
@Table (name="event")

public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank(message = "Titre est obligatoire")
    @Pattern(regexp = "([A-Za-zéàè:' ]+)", message = "titre doit contenir des caractéres alphabitiques")
    private String title;

    @Column(length = 100000)
    @NotBlank(message = "Description est obligatoire")
    private String description;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @NotNull(message = "Date et horaire sont obligatoire")
    private String datetime;

    @Column(length = 100000)
    @NotNull(message = "Photo est obligatoire")
    private String photo;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

       public Event()
       {

       }

    public Event(@NotBlank(message = "Titre est obligatoire") @Pattern(regexp = "([A-Za-zéàè:' ]+)", message = "titre doit contenir des caractéres alphabitiques") String title, @NotBlank(message = "Description est obligatoire") String description, @FutureOrPresent(message = "Veuillez sélectionner une date présente ou future") @NotNull(message = "date de début est obligatoire") String datetime, @NotNull(message = "photo est obligatoire") String photo) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.datetime = datetime;
        this.photo = photo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}


